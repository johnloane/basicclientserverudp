//Server for UDP based socket

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>

#pragma comment(lib, "Ws2_32.lib")

#define PORT 50000

int main()
{
	WSADATA WSAData;
	char sender[INET_ADDRSTRLEN];

	SOCKET server;

	SOCKADDR_IN serverAddr, clientAddr;

	bool isRunning = true;

	//start the socket library - this is windows specific
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	//create my UDP socket that I will use for communication
	server = socket(AF_INET, SOCK_DGRAM, 0);
	//want the server to accept connections from any network interface
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);

	//bind the socket to the address that I have just set up
	bind(server, (sockaddr*)&serverAddr, sizeof(serverAddr));

	char buffer[1024];
	int clientAddrSize = sizeof(clientAddr);

	while (isRunning)
	{
		std::cout << "Listening for incoming data....." << std::endl;
		recvfrom(server, buffer, sizeof(buffer), 0, (sockaddr*)&clientAddr, &clientAddrSize);
		InetNtop(AF_INET, &clientAddr.sin_addr, sender, INET_ADDRSTRLEN);
		std::cout << "Client's ip address is " << sender << std::endl;
		std::cout << "Client's port address is " << clientAddr.sin_port << std::endl;
		std::cout << "Client says: " << buffer << std::endl;
		memset(buffer, 0, sizeof(buffer));
	}
	closesocket(server);
	std::cout << "Finished now" << std::endl;
	WSACleanup();
	std::cin.ignore();


}

