//Client for UDP based socket

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <iostream>

#pragma comment(lib, "Ws2_32.lib")

#define PORT 50000

int main()
{
	WSADATA WSAData;
	SOCKET client;
	SOCKADDR_IN serverAddr;
	char buffer[1024];

	bool isRunning = true;

	//start the socket library - this is windows specific
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	//create my UDP socket that I will use for communication
	client = socket(AF_INET, SOCK_DGRAM, 0);
	
	InetPton(AF_INET, "127.0.0.1", &serverAddr.sin_addr);
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);

	while (isRunning)
	{
		std::cout << "Enter a message to send> ";
		std::cin.getline(buffer, 1024);
		sendto(client, buffer, sizeof(buffer), 0, (sockaddr*)&serverAddr, sizeof(serverAddr));
		std::cout << "Message sent " << std::endl;
	}
	closesocket(client);
	std::cout << "Finished now" << std::endl;
	WSACleanup();
	std::cin.ignore();


}

